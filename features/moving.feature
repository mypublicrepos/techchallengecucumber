Feature: Moving
	Scenario: Hoover stays within the room limits
		Given hoover's initial position [0,0]
		And The moving instructions are
		| Instructions |
		| NNNNNNN |
        | SSSSSSS |
		| EEEEEEE |
        | WWWWWWW |
		And The room size is [5,5]
		When The device follows the table of instructions
		And The final position is
		| x | y |
		| 0 | 4 |
        | 0 | 0 |
		| 4 | 0 |
        | 0 | 0 |
