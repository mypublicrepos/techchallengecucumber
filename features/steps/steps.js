	const { Given, When, Then } = require("@cucumber/cucumber");
	const { spec, expect } = require ("pactum");
  const { assertThat, is } = require('hamjest')
  const assert = require('assert');

        Given("hoover\'s initial position [{int},{int}]", function (x, y) {
          this.coords = [x,y]
        });

        Given("The moving instructions are {string}", function (instructions) {
          this.instructions = instructions
        });

        Given('The moving instructions are', function (dataTable) {
          this.instructionsTable = dataTable.rows()
        });

       
        Given('The room size is [{int},{int}]', function (x, y) {
          this.roomSize = [5,5]
	      });

        Given('Patch(es) of dirt is/are located at', function (dataTable) {
          this.patches = dataTable.rows()
	      });

        When('The device follows the moving instructions', async function () {
          const url = "http://localhost:8080/v1/cleaning-sessions";
          let data = {
            "roomSize" : this.roomSize,
            "coords" : this.coords,
            "patches" : this.patches,
            "instructions" : this.instructions
          }
          this.response = await spec().post(url).withJson(data);
        });

        When('The device follows the table of instructions', async function () {
          const url = "http://localhost:8080/v1/cleaning-sessions";
          this.responseTable = []
          let data = {
            "roomSize" : this.roomSize,
            "coords" : this.coords,
            "patches" : [[0,0]],
            "instructions" : []
          }

          for(let i =0; i<this.instructionsTable.length; i++){
            data.instructions = this.instructionsTable[i][0]
            const response = await spec().post(url).withJson(data)
            this.responseTable.push(response.body.coords)
          }
        });

        
        Then('The {int} patches are clean', function (dirt) {
          expect(this.response).should.have.status(200);
          assertThat(this.response.body.patches, is(dirt))
        });

        Then('The final position is [{int},{int}]', function ( x,y) {
          expect(this.response).should.have.status(200);
          assertThat(this.response.body.coords ,is([x,y]))
        });

        Then('The final position is', function (dataTable) {
          //let actualPositionTable = this.responseTable['coords'].messagesHeard().map(message => [message])
          this.expectedFinalPositionTable = dataTable.rows()
          console.log(this.expectedFinalPositionTable)
          console.log(this.responseTable)
          assert.deepEqual(this.expectedFinalPositionTable, this.responseTable)
        });


