# Installation 
```bash
npm install
```
This will install all depencies needed for the project

## Usage

To run the tests just execute

```bash
npm test
```

## Usage
If you want to obtain a detail report of the test execution run

```bash
CUCUMBER_PUBLISH_ENABLED=true npm test
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.
