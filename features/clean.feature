Feature: Clean
	Scenario: Hoover cleans dirt after passing through the location
		Given hoover's initial position [0,0]
		And The moving instructions are "NENENENE"
		And The room size is [5,5]
		And Patches of dirt are located at
		| x | y |
		| 0 | 0 |
        | 1 | 1 |
		| 2 | 2 |
        | 3 | 3 |
		| 4 | 4 |
		When The device follows the moving instructions
		Then The 5 patches are clean
		And The final position is [4,4]

	Scenario: Hoover does not clean twice the same dirty patch
		Given hoover's initial position [0,0]
		And The moving instructions are "NENENENESWSWSWSW"
		And The room size is [5,5]
		And Patches of dirt are located at
		| x | y |
		| 0 | 0 |
        | 1 | 1 |
		| 2 | 2 |
        | 3 | 3 |
		| 4 | 4 |
		When The device follows the moving instructions
		Then The 5 patches are clean
		And The final position is [0,0]